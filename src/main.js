var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var resolution = 16
var maxSize = 0.125
var dimension = 13
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < dimension; i++) {
    for (var j = 0; j < resolution; j++) {
      push()
      translate(
        windowWidth * 0.5 + (boardSize * maxSize + boardSize * maxSize * 2 * (1 - i / dimension)) * sin(frame + i * 0.05 * cos(frame) + Math.PI * ((i % 2) * (1 / resolution)) + Math.PI * 2 * (j / resolution)),
        windowHeight * 0.5 + (boardSize * maxSize + boardSize * maxSize * 2 * (1 - i / dimension)) * cos(frame + i * 0.05 * sin(frame) + Math.PI * ((i % 2) * (1 / resolution)) + Math.PI * 2 * (j / resolution))
      )
      fill(255 * (i % 2))
      noStroke()
      ellipse(0, 0, 0.8 * boardSize * maxSize * (1 - i / dimension))
      pop()
    }
  }

  frame += deltaTime * 0.001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
